﻿using System;
using System.Windows.Forms;

namespace GUI
{
	/// <summary>
	/// Form that displays information about the app.
	/// </summary>
	public partial class AboutForm : Form
	{
		public AboutForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Opens link to repo in default browser.
		/// </summary>
		/// <param name="sender">Event sender, linkLabel1.</param>
		/// <param name="e">Event arguments.</param>
		private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start(linkLabel1.Text);
		}
	}
}
