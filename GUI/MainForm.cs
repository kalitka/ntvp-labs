﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;
using Model;

namespace GUI
{
	/// <summary>
	/// Main form of the app. Houses DataGridView with data, menu and control buttons.
	/// </summary>
	public partial class MainForm : Form
	{
		/// <summary>
		/// List to hold all figures in.
		/// </summary>
		private BindingList<IGeometricFigure> _figures = new BindingList<IGeometricFigure>();
		
		/// <summary>
		/// A string that holds currently open file.
		/// </summary>
		private string _fileName;
		
		/// <summary>
		/// Whether current file was modified since last save.
		/// </summary>
		private bool _modified;
		
		/// <summary>
		/// Our glorious app's title shown in title bar after file name.
		/// </summary>
		public const String APP_NAME = "GRAPHICAL USER INTERFACE";
				
		/// <summary>
		/// Constructor of the main form, bind data from list to DataGridView.
		/// </summary>
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			#if DEBUG
			RndButton.Visible = true;
			#endif
			BindData();
			
			// we have an empty nameless file at the start 
			_fileName = "";
			_modified = false;
			
			// everybody stand back, i know what lambda is
			_figures.ListChanged += (object sender, ListChangedEventArgs e) => {_modified = true; UpdateCaption(); DataGridViewRowsModified(sender, null); };
			UpdateCaption();
		}
		
		/// <summary>
		/// Binds data from (some kind of) list to our DataGridView on UI
		/// see https://stackoverflow.com/questions/1228539/
		/// </summary>
		private void BindData()
		{
			dataGridView.AutoGenerateColumns = false;
			
			DataGridViewCell cell = new DataGridViewTextBoxCell();
			var columnType = new DataGridViewTextBoxColumn() 
			{
				CellTemplate = cell,
				Name = "Type column",
				HeaderText = "Type",
				DataPropertyName = "Type"
			};
			
			var columnCenter = new DataGridViewTextBoxColumn() 
			{
				CellTemplate = cell,
				Name = "Center column",
				HeaderText = "Center",
				DataPropertyName = "Center"
			};
			
			var columnArea = new DataGridViewTextBoxColumn() 
			{
				CellTemplate = cell,
				Name = "Area column",
				HeaderText = "Area",
				DataPropertyName = "Area"
			};
			
			var columnPerimeter = new DataGridViewTextBoxColumn() 
			{
				CellTemplate = cell,
				Name = "Perimeter column",
				HeaderText = "Perimeter",
				DataPropertyName = "Perimeter"
			};
			
			dataGridView.Columns.Add(columnType);
			dataGridView.Columns.Add(columnCenter);
			dataGridView.Columns.Add(columnArea);
			dataGridView.Columns.Add(columnPerimeter);
			
			dataGridView.DataSource = _figures;
		}
		
		/// <summary>
		/// Adds random figures to "database".
		/// </summary>
		/// <param name="count">Number of figures to add.</param>
		private void AddRandomFigures(int count)
		{
			var rnd = new RandomFigure();
			for (int i = 0; i < count; i++) 
			{
				_figures.Add(rnd.NextFigure());
			}
		}
		
		/// <summary>
		/// Handles 'Add random data' click. Adds 10 random figures to list.
		/// </summary>
		/// <param name="sender">Event sender, RndButton.</param>
		/// <param name="e">Event arguments.</param>
		private void RndButtonClick(object sender, EventArgs e)
		{
			AddRandomFigures(10);
		}
		
		/// <summary>
		/// Handles 'Remove current' click
		/// </summary>
		/// <param name="sender">Event sender, RemoveButton.</param>
		/// <param name="e">Event arguments.</param>
		private void RemoveButtonClick(object sender, EventArgs e)
		{
			DeleteSelection();
		}
		
		/// <summary>
		/// Deletes currently selected row in DataGridView and in list,
		/// if any is selected. Otherwise, does nothing.
		/// </summary>
		private void DeleteSelection()
		{
			if (dataGridView.SelectedRows.Count > 0) 
			{
				var index = dataGridView.SelectedRows[0].Index;
				_figures.RemoveAt(index);
			}
		}
		
		/// <summary>
		/// Handles File->Quit, Alt-F4. Quits the program.
		/// </summary>
		/// <param name="sender">Event sender, QuitToolStripMenuItem.</param>
		/// <param name="e">Event arguments.</param>
		private void QuitToolStripMenuItemClick(object sender, EventArgs e)
		{
			Close();
		}
		
		/// <summary>
		/// Enables/Disables 'Remove' and 'Edit' buttons based on selection in DataGridView.
		/// Also loads current figure into FigureEditControl
		/// </summary>
		/// <param name="sender">Event sender, dataGridView.</param>
		/// <param name="e">Event arguments.</param>
		private void DataGridViewRowsModified(object sender, EventArgs e)
		{
			removeButton.Enabled = dataGridView.SelectedRows.Count > 0;
			editButton.Enabled = dataGridView.SelectedRows.Count > 0;
			if (dataGridView.SelectedRows.Count > 0)
			{
				var index = dataGridView.SelectedRows[0].Index;
				figureEditControl1.Figure = _figures[index];
			}
		}
		
		/// <summary>
		/// Handles Help->About. Opens AboutForm as modal window.
		/// </summary>
		/// <param name="sender">Event sender, AboutToolstripMenuItem.</param>
		/// <param name="e">Event arguments.</param>
		private void AboutToolStripMenuItemClick(object sender, EventArgs e)
		{
			var af = new AboutForm();
			// ShowDialog instead of Show for modality
			af.ShowDialog();
		}
		
		/// <summary>
		/// Handles 'Add' button click. Opens empty FigureForm.
		/// </summary>
		/// <param name="sender">Event sender, AddButton.</param>
		/// <param name="e">Event arguments.</param>
		private void AddButtonClick(object sender, EventArgs e)
		{
			var ff = new FigureForm();
			if (ff.ShowDialog() == DialogResult.OK)
			{
				_figures.Add(ff.Result);
			}
		}
		
		/// <summary>
		/// Handles 'Edit' button click. Opens FigureForm with selected figure preloaded.
		/// </summary>
		/// <param name="sender">Event sender, EditButton.</param>
		/// <param name="e">Event arguments.</param>
		private void EditButtonClick(object sender, EventArgs e)
		{
			if (dataGridView.SelectedRows.Count > 0) 
			{
				var index = dataGridView.SelectedRows[0].Index;
				var ff = new FigureForm(_figures[index]);
				if (ff.ShowDialog() == DialogResult.OK) 
				{
					_figures[index] = ff.Result;
					_modified = true;
				}
			}
			
		}
		
		/// <summary>
		/// Updates form caption with current filename, modified status and app title.
		/// </summary>
		private void UpdateCaption()
		{
			var unsaved_char = _modified ? "*" : "";
			Text = String.Format("{0}{1} — {2}", _fileName, unsaved_char, APP_NAME);
		}
		
		/// <summary>
		/// Saves current data to _fileName if needed. Calls SaveAs() to get a valid name when it's blank.
		/// </summary>
		private void Save()
		{
			if (_fileName.Equals("")) 
			{
				SaveAs();
			} 
			else 
			{
				if (_modified) 
				{
					FigureIO.SaveToFile(_figures, _fileName);
					_modified = false;
					UpdateCaption();
				}
			}
		}
		
		/// <summary>
		/// Sets _fileName to a user-chosen file, then calls Save() to write to it.
		/// </summary>
		private void SaveAs()
		{
			if (saveFileDialog.ShowDialog() == DialogResult.OK) 
			{
				_fileName = saveFileDialog.FileName;
				Save();
			}
		}
		
		/// <summary>
		/// Loads selected file as new data.
		/// </summary>
		/// <param name="filename">Path to file to open.</param>
		private void Open(String filename)
		{
			try
			{
				// we can't directly replace _figures with a new BindingList, there's
				// a lot of customisation that goes right through the window and breaks
				// stuff on replacement
				_figures.Clear();
				foreach (var elem in FigureIO.LoadFormFile(filename))
				{
					_figures.Add(elem);
				}
				
			}
			catch (FileFormatException e)
			{
				MessageBox.Show(e.Message, "DATA ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
				_figures.Clear();
				_fileName = "";
			}
			_modified = false;
			UpdateCaption();
		}
		
		/// <summary>
		/// Cleans up data to an empty state.
		/// </summary>
		private void New()
		{
			_figures.Clear();
			_fileName = "";
			_modified = false;
			UpdateCaption();
		}
		
		/// <summary>
		/// Handles File->New click. New() wrapper that asks user for confirmation when necessary
		/// </summary>
		/// <param name="sender">Event sender, NewToolStripMenuItem.</param>
		/// <param name="e">Event arguments.</param>
		private void NewToolStripMenuItemClick(object sender, EventArgs e)
		{
			// Asking for confirmation when file is modified
			// when it's not modified, also proceeds
			if (!_modified || (_modified && AskForConfirmation()))
			{
				New();
			}
		}
		
		/// <summary>
		/// Handles File->Save click.
		/// </summary>
		/// <param name="sender">Event sender, saveToolStripMenuItem.</param>
		/// <param name="e">Event arguments.</param>
		private void SaveToolStripMenuItemClick(object sender, EventArgs e)
		{
			Save();
		}
		
		/// <summary>
		/// Handles File->Save As click.
		/// </summary>
		/// <param name="sender">Event sender, saveAsToolStripMenuItem.</param>
		/// <param name="e">Event arguments.</param>
		private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
		{
			SaveAs();
		}
		
		/// <summary>
		/// Handles File->Open click. Asks for saving unsaved changes if necessary,
		/// then opens a file if provied via openFileDialog.
		/// </summary>
		/// <param name="sender">Event sender, OpenToolStripMenuItem.</param>
		/// <param name="e">Event arguments.</param>
		private void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			// Should probably refactor to be consistent with other handlers which have way less code
			
			// confirmation ask if needed
			if (!_modified || (_modified && AskForConfirmation()))
			{
				if (openFileDialog.ShowDialog() == DialogResult.OK) 
				{
					var newFileName = openFileDialog.FileName;
					// don't reload file if it's not modified
					if (!newFileName.Equals(_fileName) || _modified) 
					{
						_fileName = newFileName;
						Open(_fileName);
					}
				}
			}
		}
		
		/// <summary>
		/// Warns user that current file is unsaved, asks if we have to save it first.
		/// Returns bool indicating whether we can proceed with our task.
		/// </summary>
		/// <returns>True when user selected either 'Yes' or 'No', false if 'Cancel' was selected.</returns>
		private bool AskForConfirmation()
		{
			var result = MessageBox.Show("It appears that currently opened file has some unsaved changes in it. Would you like to save it before proceeding?", "Here's a question!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			switch (result) 
			{
				case DialogResult.Yes:
					// save and continue
					Save();
					return true;
				case DialogResult.No:
					// don't save, but continue
					return true;
				default:
					// don't continue
					return false;
			}
		}
		
		/// <summary>
		/// Handles window closing, if there are any usaved changes,
		/// asks user whether he wants to continue.
		/// </summary>
		/// <param name="sender">Event sender, MainForm.</param>
		/// <param name="e">Event arguments.</param>
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			if (!_modified || (_modified && AskForConfirmation())) 
			{
				return;
			}
			e.Cancel = true;
		}
		
	}
}
