﻿using System;
using Model;
using NUnit.Framework;
using System.Windows;

namespace UnitTests.Model
{
	/// <summary>
	/// A class to test Rectangle methods and/or properties.
	/// </summary>
	[TestFixture]
	public class RectangleTest
	{
		
		/// <summary>
		/// Tests Rectangle constructor with topleft and bottomright points.
		/// </summary>
		/// <param name="x1">X coordinate of topleft point.</param>
		/// <param name="y1">Y coordinate of topleft point.</param>
		/// <param name="x2">X coordinate of bottomright point.</param>
		/// <param name="y2">Y coordinate of bottomright point.</param>
		[Test]
		[TestCase(-3, 3, 3, -3, TestName = "Regular test.")]
		[TestCase(4, 4, -4, -4, ExpectedException = typeof(ArgumentException), TestName = "Topleft on the right test.")]
		[TestCase(-5, -5, 5, 5, ExpectedException = typeof(ArgumentException), TestName = "Topleft on the bottom test.")]
		[TestCase(6, -6, -6, 6, ExpectedException = typeof(ArgumentException), TestName = "Topleft on both right and bottom test.")]
		[TestCase(-1e9, 1e9 - 23, 0, 34.34, TestName = "Limits test.")]
		[TestCase(0, 0, 1e9, -1e9, TestName = "Maximum possible width and height test.")]
		[TestCase(double.NaN, 45, 1e9, -55, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		[TestCase(-1e9, 45, 1e9, -55, ExpectedException = typeof(ArgumentException), TestName = "Width too big test.")]
		[TestCase(-4, 5.5e8, 5, -5.6e8, ExpectedException = typeof(ArgumentException), TestName = "Height too big test.")]
		[TestCase(-1e9 - 20, 1e9 + 45, -1e9 - 15, 1e9 + 44, ExpectedException = typeof(ArgumentException), TestName = "Center out of limits test.")]
		public void TwoPointConstructorTest(double x1, double y1, double x2, double y2)
		{
			var p1 = new Point(x1, y1);
			var p2 = new Point(x2, y2);
			var rectangle = new Rectangle(p1, p2);
		}
		
		/// <summary>
		/// Tests Rectangle constructor with topleft point, width and height.
		/// </summary>
		/// <param name="left">X coordinate of topleft point.</param>
		/// <param name="top">Y coordinate of topleft point.</param>
		/// <param name="width">Width of the rectangle.</param>
		/// <param name="height">Height of the rectangle.</param>
		[Test]
		[TestCase(0, 0, 23, 45.67, TestName = "Regular test.")]
		[TestCase(0, 0, 0, 12.45, ExpectedException = typeof(ArgumentException), TestName = "Zero width test.")]
		[TestCase(0, 0, -12, 123, ExpectedException = typeof(ArgumentException), TestName = "Negative width test.")]
		[TestCase(0, 0, 1e10, 67, ExpectedException = typeof(ArgumentException), TestName = "Too big width test.")]
		[TestCase(0, 0, double.NaN, 23, ExpectedException = typeof(ArgumentException), TestName = "NaN width test.")]
		[TestCase(0, 0, 0.9e9, 0.4, TestName = "Near limit width test.")]
		[TestCase(0, 0, 234.4, 0, ExpectedException = typeof(ArgumentException), TestName = "Zero height test.")]
		[TestCase(0, 0, 56, double.MinValue, ExpectedException = typeof(ArgumentException), TestName = "Negative height test.")]
		[TestCase(0, 0, 342, double.MaxValue, ExpectedException = typeof(ArgumentException), TestName = "Too big height test.")]
		[TestCase(0, 0, 34, double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN height test.")]
		[TestCase(0, 0, 34, 0.99e9, TestName = "Near limit height test.")]
		[TestCase(1e10, 0, 200, 400, ExpectedException = typeof(ArgumentException), TestName = "Center out of bounds test.")]
		public void TopLeftWidthHeightConstructorTest(double left, double top, double width, double height)
		{
			var tl = new Point(left, top);
			var rectangle = new Rectangle(tl, width, height);
		}
		
		/// <summary>
		/// Tests Rectangle constructor with width, height and center point.
		/// </summary>
		/// <param name="x">X coordinate of center point.</param>
		/// <param name="y">Y coordinate of center point.</param>
		/// <param name="width">Width of the rectangle.</param>
		/// <param name="height">Height of the rectangle.</param>
		[Test]
		[TestCase(0, 0, 23, 45.67, TestName = "Regular test.")]
		[TestCase(0, 0, 0, 12.45, ExpectedException = typeof(ArgumentException), TestName = "Zero width test.")]
		[TestCase(0, 0, -12, 123, ExpectedException = typeof(ArgumentException), TestName = "Negative width test.")]
		[TestCase(0, 0, 1e10, 67, ExpectedException = typeof(ArgumentException), TestName = "Too big width test.")]
		[TestCase(0, 0, double.NaN, 23, ExpectedException = typeof(ArgumentException), TestName = "NaN width test.")]
		[TestCase(0, 0, 0.9e9, 0.4, TestName = "Near limit width test.")]
		[TestCase(0, 0, 234.4, 0, ExpectedException = typeof(ArgumentException), TestName = "Zero height test.")]
		[TestCase(0, 0, 56, double.MinValue, ExpectedException = typeof(ArgumentException), TestName = "Negative height test.")]
		[TestCase(0, 0, 342, double.MaxValue, ExpectedException = typeof(ArgumentException), TestName = "Too big height test.")]
		[TestCase(0, 0, 34, double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN height test.")]
		[TestCase(0, 0, 34, 0.99e9, TestName = "Near limit height test.")]
		[TestCase(1e10, 0, 200, 400, ExpectedException = typeof(ArgumentException), TestName = "Center out of bounds test.")]
		[TestCase(1e9, -1e9, 567, 657, TestName = "Limit center test.")]
		public void CenterWidthHeightConstructorTest(double x, double y, double width, double height)
		{
			var center = new Point(x, y);
			var rectangle = new Rectangle(width, height, center);
		}
		
		/// <summary>
		/// Tests area calculation by given width and height. Position is fixed.
		/// </summary>
		/// <param name="width">Width of the rectangle.</param>
		/// <param name="height">Height of the rectangle.</param>
		/// <returns>Calculated area.</returns>
		[Test]
		[TestCase(4, 5, ExpectedResult = 20, TestName = "Trivial case.")]
		[TestCase(3, 1e9, ExpectedResult = 3e9, TestName = "Limit test.")]
		[TestCase(0, 5, ExpectedException = typeof(ArgumentException), TestName = "Zero width test.")]
		[TestCase(double.PositiveInfinity, 5, ExpectedException = typeof(ArgumentException), TestName = "Infinite width test.")]
		[TestCase(4, -8, ExpectedException = typeof(ArgumentException), TestName = "Negative height test.")]
		[TestCase(4, double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN height test.")]
		public double RectangleAreaTest(double width, double height)
		{
			var center = new Point(0, 0);
			var rectangle = new Rectangle(width, height, center);
			return rectangle.Area;
		}
		
		/// <summary>
		/// Tests perimeter calculation by given width and height. Position is fixed.
		/// </summary>
		/// <param name="width">Width of the rectangle.</param>
		/// <param name="height">Height of the rectangle.</param>
		/// <returns>Calculated perimeter.</returns>
		[Test]
		[TestCase(4, 5, ExpectedResult = 18, TestName = "Trivial case.")]
		[TestCase(1e9 - 2, 4, ExpectedResult = 2 * (1e9 + 2), TestName = "Near limit test.")]
		[TestCase(0, 5, ExpectedException = typeof(ArgumentException), TestName = "Zero width test.")]
		[TestCase(double.PositiveInfinity, 5, ExpectedException = typeof(ArgumentException), TestName = "Infinite width test.")]
		[TestCase(4, -8, ExpectedException = typeof(ArgumentException), TestName = "Negative height test.")]
		[TestCase(4, double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN height test.")]
		public double RectanglePerimeterTest(double width, double height)
		{
			var center = new Point(0, 0);
			var rectangle = new Rectangle(width, height, center);
			return rectangle.Perimeter;
		}
		
		/// <summary>
		/// Center property test.
		/// </summary>
		/// <param name="x">X coordinate to set for center.</param>
		/// <param name="y">Y coordinate to set for center.</param>
		/// <returns>Newly set center point as array {X, Y}.</returns>
		[Test]
		[TestCase(45, -0.8, ExpectedResult = new double[] {45, -0.8}, TestName = "Trivial case.")]
		[TestCase(1e9 - 78, -1e9 + 87, ExpectedResult = new double[] {1e9 - 78, -1e9 + 87}, TestName = "Near limits test.")]
		[TestCase(-1e9, 1e9, ExpectedResult = new double[] {-1e9, 1e9}, TestName = "Limits test.")]
		[TestCase(0, 1e9 + 0.1, ExpectedException = typeof(ArgumentException), TestName = "Out of limits test.")]
		[TestCase(double.NaN, 0.9, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public double[] RectangleCenterTest(double x, double y)
		{
			var rectangle = new Rectangle(4, 4, new Point(0, 0));
			rectangle.Center = new Point(x, y);
			return new double[] {rectangle.Center.X, rectangle.Center.Y};
		}
		
		// there is no point to test Width and Height properties separately,
		// as they are extensively* tested in CenterWidthHeightConstructorTest 
		// and TopLeftWidthHeightConstructorTest
		
	}
}
