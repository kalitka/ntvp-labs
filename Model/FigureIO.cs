﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model;

namespace Model
{
	/// <summary>
	/// Utility class that handles (de)serialization of IGeometricFigure to JSON.
	/// </summary>
	public static class FigureIO
	{
		/// <summary>
		/// Setting used to load three different classes with different fields with single line of code.
		/// </summary>
		private static readonly JsonSerializerSettings _settings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.All};
		
		/// <summary>
		/// Arbitrary string to distinguish valid files
		/// </summary>
		private const string _fileHeader = "GUI_DB_MODEL_FILE_VER_1.1";
		
		/// <summary>
		/// Method to serialize an IGeometricFigure's ancestor to JSON string.
		/// </summary>
		/// <param name="figure">Figure to serialize.</param>
		/// <returns>JSON string representation.</returns>
		private static string Serialize(IGeometricFigure figure)
		{
			return JsonConvert.SerializeObject(figure, _settings);
		}
		
		/// <summary>
		/// Method to deserialize IGeometricFigure from JSON string.
		/// </summary>
		/// <param name="json">JSON string to deserialize.</param>
		/// <returns>Deserialized IGeometricFigure.</returns>
		private static IGeometricFigure Deserialize(string json)
		{
			return (IGeometricFigure)JsonConvert.DeserializeObject(json, _settings);
		}
		
		/// <summary>
		/// Method to save BindingList of IGeometricfigure to file.
		/// </summary>
		/// <param name="figures">List to save.</param>
		/// <param name="filename">Name of the file to save to.</param>
		public static void SaveToFile(BindingList<IGeometricFigure> figures, string filename)
		{
			var file = new StreamWriter(filename);
			file.WriteLine(_fileHeader);
			foreach (var figure in figures) 
			{
				var jsonString = Serialize(figure);
				file.WriteLine(jsonString);
			}
			file.Close();
		}
		
		public static List<IGeometricFigure> LoadFormFile(string filename)
		{
			var file = new StreamReader(filename);
			var header = file.ReadLine();
			var ret = new List<IGeometricFigure>();
			if (!header.Equals(_fileHeader)) 
			{
				file.Close();
				throw new FileFormatException("File header is missing or corrupt.");
			}
			while (!file.EndOfStream) 
			{
				try 
				{
					var line = file.ReadLine();
					var figure = Deserialize(line);
					ret.Add(figure);
				} 
				catch
				{
					file.Close();
					throw new FileFormatException("Error while reading data in file.");
				}
			}
			file.Close();
			return ret;
		}
		
	}
}
