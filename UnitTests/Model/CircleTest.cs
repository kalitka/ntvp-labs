﻿using System;
using Model;
using System.Windows;
using NUnit.Framework;

namespace UnitTests.Model
{
	/// <summary>
	/// A class to test Circle methods and/or properties.
	/// </summary>
	[TestFixture]
	public class CircleTest
	{
		
		// NUnit 3 is acting weird on SharpDevelop, for instance
		// TestCase's Expected result is null isntead of provided value for
		// some esoteric reason. Last version of NUnit 2 is used.
		
		/// <summary>
		/// Circle constructor test. Also tests Radius property.
		/// </summary>
		/// <param name="radius">Radius to pass into the constructor.</param>
		[Test]
		[TestCase(1, TestName = "Test construction of circle with radius 1.")]
		[TestCase(23.4, TestName = "Non-integer radius test.")]
		[TestCase(double.Epsilon, TestName = "Epsilon radius test.")]
		[TestCase(1e9, TestName = "Maximum allowed radius test.")]
		[TestCase(1e9-1, TestName = "Almost maximum allowed radius test.")]
		// test failures start here
		[TestCase(double.MaxValue, ExpectedException = typeof(ArgumentException), TestName = "Maximum possible radius test.")]
		[TestCase(double.PositiveInfinity, ExpectedException = typeof(ArgumentException), TestName = "Infinity radius test.")]
		[TestCase(-1, ExpectedException = typeof(ArgumentException), TestName = "Negative radius test.")]
		[TestCase(1e9+1, ExpectedException = typeof(ArgumentException), TestName = "Slightly bigger than maximum allowed radius test.")]
		[TestCase(0, ExpectedException = typeof(ArgumentException), TestName = "Zero radius test.")]
		[TestCase(double.MinValue, ExpectedException = typeof(ArgumentException), TestName = "Minimum possible radius test.")]
		[TestCase(double.NegativeInfinity, ExpectedException = typeof(ArgumentException), TestName = "Negative infinity radius test.")]
		[TestCase(double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public void CircleInitTest(double radius)
		{
			var circle = new Circle(radius);
		}
		
		/// <summary>
		/// Tests Circle constructor with fixed radius and Center property by extention
		/// </summary>
		/// <param name="x">X coordinate of point.</param>
		/// <param name="y">Y coordinate of point.</param>
		[Test]
		[TestCase(0, 0, TestName = "Trivial test.")]
		[TestCase(1e9, -1e9, TestName = "Limits test.")]
		[TestCase(1e9 - 1, -1e9 + 2, TestName = "Near limits test.")]
		// test failures start here
		[TestCase(double.PositiveInfinity, double.NegativeInfinity, ExpectedException = typeof(ArgumentException), TestName = "Infinity test")]
		[TestCase(1e9 + 1, -1e9 - 1, ExpectedException = typeof(ArgumentException), TestName = "Out of bounds test.")]
		[TestCase(double.NaN, double.MaxValue, ExpectedException = typeof(ArgumentException), TestName = "Other values test.")]
		public void CircleInitCenterTest(double x, double y)
		{
			var radius = 10; // whatever, we've already tested that
			var p = new Point(x, y);
			var circle = new Circle(radius, p);
		}
		
		/// <summary>
		/// Tests circle area calculation.
		/// </summary>
		/// <param name="radius">Radius to pass to the Circle.</param>
		/// <returns>Calculated area.</returns>
		[Test]
		[TestCase(1, ExpectedResult = Math.PI, TestName = "Area with radius 1 test.")]
		[TestCase(28.06, ExpectedResult = Math.PI * 28.06 * 28.06, TestName = "28.06 radius area test.")]
		[TestCase(1e9, ExpectedResult = Math.PI * 1e18, TestName = "Maximum allowed radius test.")]
		[TestCase(1e9-1, ExpectedResult = Math.PI * (1e9 - 1) * (1e9 - 1), TestName = "Almost maximum allowed radius test.")]
		// test failures start here
		[TestCase(double.PositiveInfinity, ExpectedException = typeof(ArgumentException), TestName = "Infinity radius test.")]
		[TestCase(-1, ExpectedException = typeof(ArgumentException), TestName = "Negative radius test.")]
		[TestCase(1e9+1, ExpectedException = typeof(ArgumentException), TestName = "Slightly bigger than maximum allowed radius test.")]
		[TestCase(0, ExpectedException = typeof(ArgumentException), TestName = "Zero radius test.")]
		[TestCase(double.MinValue, ExpectedException = typeof(ArgumentException), TestName = "Minimum possible radius test.")]
		[TestCase(double.NegativeInfinity, ExpectedException = typeof(ArgumentException), TestName = "Negative infinity radius test.")]
		[TestCase(double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public double CircleAreaTest(double radius)
		{
			return new Circle(radius).Area;
		}
		
		/// <summary>
		/// Tests circle perimeter calculation.
		/// </summary>
		/// <param name="radius">Radius to pass to the Circle.</param>
		/// <returns>Calculated perimeter.</returns>
		[Test]
		[TestCase(1, ExpectedResult = 2 * Math.PI, TestName = "Radius 1 perimeter test.")]
		[TestCase(476.456, ExpectedResult = 2 * Math.PI * 476.456, TestName = "Perimeter test.")]
		[TestCase(1e9, ExpectedResult = 2e9 * Math.PI, TestName = "Maximum allowed perimeter test.")]
		[TestCase(1e9 - 2, ExpectedResult = 2 * Math.PI * (1e9 - 2), TestName = "Slightly smaller than maximum radius test.")]
		// test failures start here
		[TestCase(double.PositiveInfinity, ExpectedException = typeof(ArgumentException), TestName = "Infinity radius test.")]
		[TestCase(-1, ExpectedException = typeof(ArgumentException), TestName = "Negative radius test.")]
		[TestCase(1e9+1, ExpectedException = typeof(ArgumentException), TestName = "Slightly bigger than maximum allowed radius test.")]
		[TestCase(0, ExpectedException = typeof(ArgumentException), TestName = "Zero radius test.")]
		[TestCase(double.MinValue, ExpectedException = typeof(ArgumentException), TestName = "Minimum possible radius test.")]
		[TestCase(double.NegativeInfinity, ExpectedException = typeof(ArgumentException), TestName = "Negative infinity radius test.")]
		[TestCase(double.NaN, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public double CirclePerimeterTest(double radius)
		{
			return new Circle(radius).Perimeter;
		}
		
		// there's no sense to test Radius and Center properties separately,
		// as they're tested in constructor tests
		
	}
}
