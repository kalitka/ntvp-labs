﻿using System;
using Model;
using System.Windows;
using NUnit.Framework;

namespace UnitTests.Model
{
	/// <summary>
	/// A class to test Triangle methods and/or properties.
	/// </summary>
	[TestFixture]
	public class TriangleTest
	{	
		// I found no simple way to use point as testcase attribute
		// (both Point(0, 0) and new Point(0, 0) cause compiler errors)
		// so here comes the dumb approach
		// what am i doing with my life
		
		/// <summary>
		/// Triangle constructor (ugly) test.
		/// </summary>
		/// <param name="ax">X coordinate of first point.</param>
		/// <param name="ay">Y coordinate of first point.</param>
		/// <param name="bx">X coordinate of second point.</param>
		/// <param name="by">Y coordinate of second point.</param>
		/// <param name="cx">X coordinate of third point.</param>
		/// <param name="cy">Y coordinate of third point.</param>
		[Test]
		[TestCase(0, 0, 1, 2, -4, -6.7, TestName = "Regular test.")]
		[TestCase(1, 2, 3, 4, 5, 6, ExpectedException = typeof(ArgumentException), TestName = "Degenerate triangle test.")]
		[TestCase(1e9, 1e9, -1e9, -1e9, 1e9, 1e9-1, TestName = "Limits test.")]
		[TestCase(1e9 + 1, 0, 0, 250, 0, -250, ExpectedException = typeof(ArgumentException), TestName = "Out of limits test.")]
		[TestCase(double.NaN, double.NegativeInfinity, double.PositiveInfinity, double.MaxValue, double.MinValue, 1337, ExpectedException = typeof(ArgumentException), TestName = "Invalid values test.")]
		[TestCase(1e9 - 1, 1e9 - 1, 1e9 - 3, 1e9 - 7, 1e9 - 10, 1e9 - 11, TestName = "Near limits test.")]
		[TestCase(-1e9 + 1, -1e9 + 2, -1e9 + 3, -1e9 + 4, -1e9 + 5, -1e9 + 6, ExpectedException = typeof(ArgumentException), TestName = "Degenerate triangle near limits test.")]
		public void TriangleInitTest(double ax, double ay, double bx, double by, double cx, double cy)
		{
			var pA = new Point(ax, ay);
			var pB = new Point(bx, by);
			var pC = new Point(cx, cy);
			var triangle = new Triangle(pA, pB, pC);
		}
		
		/// <summary>
		/// Triangle (ugly) GetPoints (ugly) test.
		/// </summary>
		/// <param name="ax">X coordinate of first point.</param>
		/// <param name="ay">Y coordinate of first point.</param>
		/// <param name="bx">X coordinate of second point.</param>
		/// <param name="by">Y coordinate of second point.</param>
		/// <param name="cx">X coordinate of third point.</param>
		/// <param name="cy">Y coordinate of third point.</param>
		/// <returns>The very same coordinates.</returns>
		[Test]
		[TestCase(1, 2, 3.4, 5, 6.7, 8, ExpectedResult = new double[] {1, 2, 3.4, 5, 6.7, 8}, TestName = "Regular test.")]
		[TestCase(1, 1, 2, 2, 8, 8, ExpectedException = typeof(ArgumentException), TestName = "Degenerate triangle test.")]
		[TestCase(1e9, -1e9, 250, -124357, 1e9 - 1, -1e9 + 1, ExpectedResult = new double[] {1e9, -1e9, 250, -124357, 1e9 - 1, -1e9 + 1}, TestName = "Limits test.")]
		[TestCase(2345, -456, 4.5, 0.56, -7, 1e9 + 2, ExpectedException = typeof(ArgumentException), TestName = "Out of limits test.")]
		[TestCase(double.NegativeInfinity, double.NaN, double.PositiveInfinity, double.MinValue, double.MaxValue, 123445.34566, ExpectedException = typeof(ArgumentException), TestName = "Wrong values test.")]
		public double[] GetPointsTest(double ax, double ay, double bx, double by, double cx, double cy)
		{
			var pA = new Point(ax, ay);
			var pB = new Point(bx, by);
			var pC = new Point(cx, cy);
			var triangle = new Triangle(pA, pB, pC);
			return triangle.GetPoints();
		}
		
		/// <summary>
		/// Area calcualtion test.
		/// </summary>
		/// <param name="ax">X coordinate of first point.</param>
		/// <param name="ay">Y coordinate of first point.</param>
		/// <param name="bx">X coordinate of second point.</param>
		/// <param name="by">Y coordinate of second point.</param>
		/// <param name="cx">X coordinate of third point.</param>
		/// <param name="cy">Y coordinate of third point.</param>
		/// <returns>Area of the triangle.</returns>
		[Test]
		[TestCase(0, 0, 0, 3, 4, 3, ExpectedResult = 6, TestName = "Regular test.")]
		[TestCase(1e9 - 4, 1e9 - 3, 1e9 - 4, 1e9, 1e9, 1e9, ExpectedResult = 6, TestName = "Near limits test.")]
		[TestCase(-1e9, 1e9 - 3, -1e9, 1e9, -1e9 + 4, 1e9, ExpectedResult = 6, TestName = "Near negative limit test.")]
		[TestCase(1e9 - 4, 1e9 - 3, 1e9 - 4, 1e9, 1e9, 1e9+1, ExpectedException = typeof(ArgumentException), TestName = "Out of limits test.")]
		[TestCase(double.NaN, 0, 0, 3, 4, 3, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public double TriangleAreaTest(double ax, double ay, double bx, double by, double cx, double cy)
		{
			var pA = new Point(ax, ay);
			var pB = new Point(bx, by);
			var pC = new Point(cx, cy);
			var triangle = new Triangle(pA, pB, pC);
			return triangle.Area;
		}
		
		// ^C, ^V ftw
		
		/// <summary>
		/// Perimeter calcualtion test.
		/// </summary>
		/// <param name="ax">X coordinate of first point.</param>
		/// <param name="ay">Y coordinate of first point.</param>
		/// <param name="bx">X coordinate of second point.</param>
		/// <param name="by">Y coordinate of second point.</param>
		/// <param name="cx">X coordinate of third point.</param>
		/// <param name="cy">Y coordinate of third point.</param>
		/// <returns>Perimeter of the triangle.</returns>
		[Test]
		[TestCase(0, 0, 0, 3, 4, 3, ExpectedResult = 12, TestName = "Regular test.")]
		[TestCase(1e9 - 4, 1e9 - 3, 1e9 - 4, 1e9, 1e9, 1e9, ExpectedResult = 12, TestName = "Near limits test.")]
		[TestCase(-1e9, 1e9 - 3, -1e9, 1e9, -1e9 + 4, 1e9, ExpectedResult = 12, TestName = "Near negative limit test.")]
		[TestCase(1e9 - 4, 1e9 - 3, 1e9 - 4, 1e9, 1e9, 1e9+1, ExpectedException = typeof(ArgumentException), TestName = "Out of limits test.")]
		[TestCase(double.NaN, 0, 0, 3, 4, 3, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public double TrianglePerimeterTest(double ax, double ay, double bx, double by, double cx, double cy)
		{
			var pA = new Point(ax, ay);
			var pB = new Point(bx, by);
			var pC = new Point(cx, cy);
			var triangle = new Triangle(pA, pB, pC);
			return triangle.Perimeter;
		}
		
		// Again, no way to use Point in ExpectedResult
		
		/// <summary>
		/// Center property test.
		/// </summary>
		/// <param name="x">X coordinate to set for center.</param>
		/// <param name="y">Y coordinate to set for center.</param>
		/// <returns>Newly set center point as array {X, Y}.</returns>
		[Test]
		[TestCase(234.6, -78.98, ExpectedResult = new double[] {234.6, -78.98}, TestName = "Regular test.")]
		[TestCase(1e9 - 12, -1e9 + 23, ExpectedResult = new double[] {1e9 - 12, -1e9 + 23}, TestName = "Near limits test.")]
		[TestCase(-1e9, 1e9, ExpectedResult = new double[] {-1e9, 1e9}, TestName = "Limits test.")]
		[TestCase(0, 1e9 + 34, ExpectedException = typeof(ArgumentException), TestName = "Out of limits test.")]
		[TestCase(double.NaN, 456.34, ExpectedException = typeof(ArgumentException), TestName = "NaN test.")]
		public double[] TriangleCenterTest(double x, double y)
		{
			var pA = new Point(0, 4);
			var pB = new Point(-3, -2);
			var pC = new Point(3, -2);
			var triangle = new Triangle(pA, pB, pC);
			triangle.Center = new Point(x, y);
			return new double[] {triangle.Center.X, triangle.Center.Y};
		}
		
	}
}