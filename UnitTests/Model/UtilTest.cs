﻿using System;
using Model;
using NUnit.Framework;
using System.Windows;

namespace UnitTests.Model
{
	/// <summary>
	/// Unit tests for utility class.
	/// </summary>
	[TestFixture]
	public class UtilTest
	{
		/// <summary>
		/// Test method for Util.IsValid(double)
		/// </summary>
		/// <param name="value">Value to pass to the tested method.</param>
		/// <returns>Util.IsValid(value).</returns>
		[Test]
		[TestCase(234, ExpectedResult = true, TestName = "Regular test.")]
		[TestCase(1e9 - 1, ExpectedResult = true, TestName = "Near limit test.")]
		[TestCase(1e9, ExpectedResult = true, TestName = "Limit test.")]
		[TestCase(1e9 + 1, ExpectedResult = false, TestName = "Out of the limit test.")]
		[TestCase(-1e9 + 1, ExpectedResult = true, TestName = "Near negative limit test.")]
		[TestCase(-1e9, ExpectedResult = true, TestName = "Negative limit test.")]
		[TestCase(-1e9 - 1, ExpectedResult = false, TestName = "Out of the negative limit test.")]
		[TestCase(double.NaN, ExpectedResult = false, TestName = "Nan test.")]
		[TestCase(double.MaxValue, ExpectedResult = false, TestName = "MaxValue test.")]
		[TestCase(double.NegativeInfinity, ExpectedResult = false, TestName = "Infinity test.")]
		public bool IsValidDoubleTest(double value)
		{
			return Util.IsValid(value);
		}
		
		/// <summary>
		/// Test method for Util.IsValidPositive
		/// </summary>
		/// <param name="value">Value to pass to the tested method.</param>
		/// <returns>Util.IsValidPositive(value).</returns>
		[Test]
		[TestCase(234, ExpectedResult = true, TestName = "Regular test.")]
		[TestCase(-456, ExpectedResult = false, TestName = "Negative test.")]
		[TestCase(1e9 - 1, ExpectedResult = true, TestName = "Near limit test.")]
		[TestCase(1e9, ExpectedResult = true, TestName = "Limit test.")]
		[TestCase(1e9 + 1, ExpectedResult = false, TestName = "Out of the limit test.")]
		[TestCase(-1e9 + 1, ExpectedResult = false, TestName = "Near negative limit test.")]
		[TestCase(-1e9, ExpectedResult = false, TestName = "Negative limit test.")]
		[TestCase(-1e9 - 1, ExpectedResult = false, TestName = "Out of the negative limit test.")]
		[TestCase(double.NaN, ExpectedResult = false, TestName = "Nan test.")]
		[TestCase(double.MaxValue, ExpectedResult = false, TestName = "MaxValue test.")]
		[TestCase(double.NegativeInfinity, ExpectedResult = false, TestName = "Infinity test.")]
		public bool IsValidPositiveTest(double value)
		{
			return Util.IsValidPositive(value);
		}
		
		/// <summary>
		/// Test method for Util.IsValid(Point).
		/// </summary>
		/// <param name="x">X coordinate of point.</param>
		/// <param name="y">Y coordinate of point.</param>
		/// <returns>Util.IsValid(Point(x, y)).</returns>
		[Test]
		[TestCase(2345, -102344, ExpectedResult = true, TestName = "Regular test.")]
		[TestCase(1e9 - 1, -1e9 + 1, ExpectedResult = true, TestName = "Near limits test.")]
		[TestCase(-1e9, 1e9, ExpectedResult = true, TestName = "Limits test.")]
		[TestCase(double.PositiveInfinity, double.NegativeInfinity, ExpectedResult = false, TestName = "Infinity test.")]
		[TestCase(double.MaxValue, double.MinValue, ExpectedResult = false, TestName = "Max and Min Value test.")]
		[TestCase(345.456, double.NaN, ExpectedResult = false, TestName = "NaN test.")]
		public bool IsValidPointTest(double x, double y)
		{
			var p = new Point(x, y);
			return Util.IsValid(p);
		}
	}
}
