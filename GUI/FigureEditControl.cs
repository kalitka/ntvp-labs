﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Model;

namespace GUI
{
	/// <summary>
	/// Description of FigureEditControl.
	/// </summary>
	public partial class FigureEditControl : UserControl
	{
		/// <summary>
		/// Event hander used to update "OK", "Generate" and whatever buttons
		/// without linking them.
		/// </summary>
		public event EventHandler MyValidatedEvent;
		
		/// <summary>
		/// Used to list TextBoxes that check for content > 0 as additional validation
		/// </summary>
		readonly TextBox[] CheckForPositiveTextBoxList;
		
		/// <summary>
		/// Figure property used to load IGeometricFigure to and from this control.
		/// </summary>
		public IGeometricFigure Figure
		{
			get { return ConstructFigure(); }
			set { LoadFigure(value); }
		}
		
		/// <summary>
		/// Internal read-only flag.
		/// </summary>
		private bool _readonly;
		
		/// <summary>
		/// Read-only property, turn off editing.
		/// </summary>
		public bool ReadOnly
		{
			get { return _readonly; }
			set
			{
				#if DEBUG 
				rndButton.Visible = !value;
				#endif
				// i don't even know anymore
				XTextBox.ReadOnly = value;
				YTextBox.ReadOnly = value;
				RadiusTextBox.ReadOnly = value;
				WidthTextBox.ReadOnly = value;
				HeightTextBox.ReadOnly = value;
				AXTextBox.ReadOnly = value;
				AYTextBox.ReadOnly = value;
				BXTextBox.ReadOnly = value;
				BYTextBox.ReadOnly = value;
				CXTextBox.ReadOnly = value;
				CYTextBox.ReadOnly = value;
				figureComboBox.Enabled = !value;
				_readonly = value;
			}
		}
		
		/// <summary>
		/// Property indicating whether input in this control is valid. Read-only.
		/// </summary>
		public bool IsValid
		{
			get { return ValidateData() && !ReadOnly;}
		}
		
		/// <summary>
		/// Constructor of the control.
		/// </summary>
		public FigureEditControl()
		{
			
			InitializeComponent();
			
			#if DEBUG
			if (!ReadOnly)
			{
				rndButton.Visible = true;
			}
			#endif
			CheckForPositiveTextBoxList = new [] {RadiusTextBox, WidthTextBox, HeightTextBox};
			figureComboBox.SelectedIndex = 0;
			XTextBox.Text = "0";
			YTextBox.Text = "0";
		}
		
		/// <summary>
		/// Actual method that loads figure's data into form controls.
		/// </summary>
		/// <param name="figure">Figure to get data from.</param>
		private void LoadFigure(IGeometricFigure figure)
		{
			if (figure is Circle) {
				figureComboBox.SelectedIndex = 0;
				RadiusTextBox.Text = ((Circle)figure).Radius.ToString();
			} else if (figure is Rectangle) {
				figureComboBox.SelectedIndex = 1;
				WidthTextBox.Text = ((Rectangle)figure).Width.ToString();
				HeightTextBox.Text = ((Rectangle)figure).Height.ToString();
			} else if (figure is Triangle) {
				figureComboBox.SelectedIndex = 2;
				// TODO This is ugly, I should refaсtor that
				double[] crutch = ((Triangle)figure).GetPoints();
				AXTextBox.Text = crutch[0].ToString();
				AYTextBox.Text = crutch[1].ToString();
				BXTextBox.Text = crutch[2].ToString();
				BYTextBox.Text = crutch[3].ToString();
				CXTextBox.Text = crutch[4].ToString();
				CYTextBox.Text = crutch[5].ToString();
			}
			XTextBox.Text = figure.Center.X.ToString();
			YTextBox.Text = figure.Center.Y.ToString();
		}
		
		/// <summary>
		/// Handles visibility of figure-specific panels.
		/// </summary>
		/// <param name="sender">Control that fired the event, comboBox1.</param>
		/// <param name="e">Event arguments.</param>
		private void ComboBox1SelectedValueChanged(object sender, EventArgs e)
		{
			CirclePanel.Visible = figureComboBox.SelectedIndex == 0;
			RectanglePanel.Visible = figureComboBox.SelectedIndex == 1;
			TrianglePanel.Visible = figureComboBox.SelectedIndex == 2;
			RaiseMyValidatedEvent(this, null);
		}
		
		/// <summary>
		/// Click on 'Generate random figure'. Changes form data to a random valid figure.
		/// </summary>
		/// <param name="sender">Event sender, RndButton.</param>
		/// <param name="e">Event arguments.</param>
		private void RndButtonClick(object sender, EventArgs e)
		{
			var rnd = new RandomFigure();
			LoadFigure(rnd.NextFigure());
		}

		/// <summary>
		/// Sets 'OK' button state based on data integrity on TextBox changes.
		/// </summary>
		/// <param name="sender">Event sender, all of form's TextBoxes.</param>
		/// <param name="e">Event arguments.</param>
		private void TextBoxTextChanged(object sender, EventArgs e)
		{
			RaiseMyValidatedEvent(this, null);
		}
		
		/// <summary>
		/// Checks whether TextBox data is valid: can be converted to double,
		/// and, in some cases, is greater than zero (like circle's radius).
		/// Also sets font based on result of this check.
		/// </summary>
		/// <param name="tb">TextBox to check.</param>
		/// <returns>True when data is valid, false when otherwise.</returns>
		private bool DataCheck(TextBox tb)
		{
			bool success = true;
			try 
			{
				double d = Convert.ToDouble(tb.Text);
				success = CheckForPositiveTextBoxList.Contains(tb) ? Util.IsValidPositive(d) : Util.IsValid(d);
			} 
			catch (FormatException)
			{
				success = false;
			} 
			catch (OverflowException)
			{
				success = false;
			}
			var fontStyle = success ? System.Drawing.FontStyle.Regular : System.Drawing.FontStyle.Bold;
			if (tb.Font.Style != fontStyle) {
				tb.Font = new System.Drawing.Font(tb.Font, fontStyle);
			}
			return success;
		}
		
		/// <summary>
		/// Checks all TextBoxes related to selected figure type.
		/// </summary>
		/// <returns>True when data is valid and figure can be constructed, false when otherwise.</returns>
		private bool ValidateData()
		{
			if (!DataCheck(XTextBox) || !DataCheck(YTextBox)) 
			{
				return false;
			}
			Panel panelToCheck;
			switch (figureComboBox.SelectedIndex) 
			{
				case 0:
					panelToCheck = CirclePanel;
					break;
				case 1:
					panelToCheck = RectanglePanel;
					break;
				case 2:
					panelToCheck = TrianglePanel;
					break;
				default:
					throw new NotImplementedException();
			}
			foreach (var control in panelToCheck.Controls) 
			{
				if (control is TextBox) 
				{
					if (!DataCheck((TextBox)control)) 
					{
						return false;
					}
				}
			}
			return true;
		}
		
		/// <summary>
		/// Creates an IGeometricFigure ancestor based on current form data.
		/// </summary>
		/// <returns>New geometric figure.</returns>
		private IGeometricFigure ConstructFigure()
		{
			switch (figureComboBox.SelectedIndex) 
			{
				case 0:
					return GetCircle();
				case 1:
					return GetRectangle();
				case 2:
					return GetTriangle();
				default:
					throw new IndexOutOfRangeException("Unknown figure index.");
			}
		}
		
		/// <summary>
		/// Gets center point from the form. Throws an ArgumentException when unable to.
		/// </summary>
		/// <returns>Center as a Point.</returns>
		private Point GetCenter()
		{
			try 
			{
				var cx = Convert.ToDouble(XTextBox.Text);
				var cy = Convert.ToDouble(YTextBox.Text);
				return new Point(cx, cy);
			}
			catch (FormatException) 
			{
				throw new ArgumentException("Invalid data in center textboxes.");
			}
		}
		
		/// <summary>
		/// Constructs Circle from form data. Throws an ArgumentException when unable to load radius.
		/// </summary>
		/// <returns>New Circle from form data.</returns>
		private Circle GetCircle()
		{
			var center = GetCenter();
			try 
			{
				var radius = Convert.ToDouble(RadiusTextBox.Text);
				return new Circle(radius, center);
			}
			catch (FormatException) 
			{
				throw new ArgumentException("Invalid data in radius textbox.");
			}
		}
		
		/// <summary>
		/// Constructs Rectangle from form data. Throws an ArgumentException when unable to load width and/or height.
		/// </summary>
		/// <returns>New Rectangle from form data.</returns>
		private Rectangle GetRectangle()
		{
			var center = GetCenter();
			try 
			{
				var width = Convert.ToDouble(WidthTextBox.Text);
				var height = Convert.ToDouble(HeightTextBox.Text);
				return new Rectangle(width, height, center);
			} 
			catch (FormatException) 
			{
				throw new ArgumentException("Invalid data in rectangle dimensions textboxes.");
			}
		}
		
		/// <summary>
		/// Constructs Triangle from form data. Throws an ArgumentException when unable to load any point's coordinate.
		/// </summary>
		/// <returns>New Triangle from form data.</returns>
		private Triangle GetTriangle()
		{
			try 
			{
				// https://www.youtube.com/watch?v=ebUAhGj1reM
				var x = Convert.ToDouble(AXTextBox.Text);
				var y = Convert.ToDouble(AYTextBox.Text);
				var pointA = new Point(x, y);
				x = Convert.ToDouble(BXTextBox.Text);
				y = Convert.ToDouble(BYTextBox.Text);
				var pointB = new Point(x, y);
				x = Convert.ToDouble(CXTextBox.Text);
				y = Convert.ToDouble(CYTextBox.Text);
				var pointC = new Point(x, y);
				return new Triangle(pointA, pointB, pointC);
			} 
			catch (FormatException) 
			{
				throw new ArgumentException("Invalid data in triangle points textboxes.");
			}
		}
		
		/// <summary>
		/// Event handler for validating center textboxes: moves the triangle from form,
        /// if it's valid, so it's center is really the point chosen by center textboxes.
		/// </summary>
		/// <param name="sender">Event sender, XTextBox or YTextBox.</param>
		/// <param name="e">Event arguments.</param>
		private void CenterTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (figureComboBox.SelectedIndex == 2) 
			{
				// saving to restore when exceptions are there
				var oldAX = AXTextBox.Text;
				var oldAY = AYTextBox.Text;
				var oldBX = BXTextBox.Text;
				var oldBY = BYTextBox.Text;
				var oldCX = CXTextBox.Text;
				var oldCY = CYTextBox.Text;
				
				if (ValidateData()) 
				{
					try 
					{
						var tr = GetTriangle();
						tr.Center = GetCenter();
						LoadFigure(tr);
					}
					catch (ArgumentException) 
					{
						AXTextBox.Text = oldAX;
						AYTextBox.Text = oldAY;
						BXTextBox.Text = oldBX;
						BYTextBox.Text = oldBY;
						CXTextBox.Text = oldCX;
						CYTextBox.Text = oldCY;
					}
				}
			}
		}
		
		/// <summary>
		/// Event handler for validating triangle points textboxes: if the triangle is valid,
		/// set the center to actual center of current triangle.
		/// </summary>
		/// <param name="sender">Event sender, any of the triangle textboxes.</param>
		/// <param name="e">Event arguments</param>
		private void TriangleTextBoxValidating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			var oldX = XTextBox.Text;
			var oldY = YTextBox.Text;
			if (ValidateData()) 
			{
				try 
				{
					// here's the trick: GetTriangle do not uses center to construct a triangle,
					// but sets its center anyway
					LoadFigure(GetTriangle());
				} 
				catch (ArgumentException) 
				{
					XTextBox.Text = oldX;
					YTextBox.Text = oldY;
				}
			}
		}
		
		/// <summary>
		/// Raises custom event with null check.
		/// </summary>
		/// <param name="sender">Event sender, most likely this.</param>
		/// <param name="e">Event arguments, most likely null.</param>
		private void RaiseMyValidatedEvent(object sender, EventArgs e)
		{
			if (this.MyValidatedEvent != null)
				this.MyValidatedEvent(sender, e);
		}
	}
}
