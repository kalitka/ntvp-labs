﻿
namespace GUI
{
	partial class FigureEditControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.ComboBox figureComboBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button rndButton;
		private System.Windows.Forms.Panel CirclePanel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox XTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox YTextBox;
		private System.Windows.Forms.TextBox RadiusTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel RectanglePanel;
		private System.Windows.Forms.Panel TrianglePanel;
		private System.Windows.Forms.TextBox CYTextBox;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox CXTextBox;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox BYTextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox BXTextBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox AYTextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox AXTextBox;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox HeightTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox WidthTextBox;
		private System.Windows.Forms.Label label8;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.figureComboBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.rndButton = new System.Windows.Forms.Button();
			this.CirclePanel = new System.Windows.Forms.Panel();
			this.RadiusTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.RectanglePanel = new System.Windows.Forms.Panel();
			this.HeightTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.WidthTextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.TrianglePanel = new System.Windows.Forms.Panel();
			this.CYTextBox = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.CXTextBox = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.BYTextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.BXTextBox = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.AYTextBox = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.AXTextBox = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.XTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.YTextBox = new System.Windows.Forms.TextBox();
			this.CirclePanel.SuspendLayout();
			this.RectanglePanel.SuspendLayout();
			this.TrianglePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// figureComboBox
			// 
			this.figureComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.figureComboBox.FormattingEnabled = true;
			this.figureComboBox.Items.AddRange(new object[] {
			"Circle",
			"Rectangle",
			"Triangle"});
			this.figureComboBox.Location = new System.Drawing.Point(72, 8);
			this.figureComboBox.Name = "figureComboBox";
			this.figureComboBox.Size = new System.Drawing.Size(120, 22);
			this.figureComboBox.TabIndex = 0;
			this.figureComboBox.SelectedValueChanged += new System.EventHandler(this.ComboBox1SelectedValueChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Figure type:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// rndButton
			// 
			this.rndButton.Location = new System.Drawing.Point(8, 232);
			this.rndButton.Name = "rndButton";
			this.rndButton.Size = new System.Drawing.Size(184, 23);
			this.rndButton.TabIndex = 4;
			this.rndButton.Text = "Generate random figure";
			this.rndButton.UseVisualStyleBackColor = true;
			this.rndButton.Visible = false;
			this.rndButton.Click += new System.EventHandler(this.RndButtonClick);
			// 
			// CirclePanel
			// 
			this.CirclePanel.Controls.Add(this.RadiusTextBox);
			this.CirclePanel.Controls.Add(this.label4);
			this.CirclePanel.Location = new System.Drawing.Point(8, 80);
			this.CirclePanel.Name = "CirclePanel";
			this.CirclePanel.Size = new System.Drawing.Size(184, 144);
			this.CirclePanel.TabIndex = 5;
			// 
			// RadiusTextBox
			// 
			this.RadiusTextBox.Location = new System.Drawing.Point(64, 0);
			this.RadiusTextBox.MaxLength = 40;
			this.RadiusTextBox.Name = "RadiusTextBox";
			this.RadiusTextBox.Size = new System.Drawing.Size(120, 20);
			this.RadiusTextBox.TabIndex = 11;
			this.RadiusTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 20);
			this.label4.TabIndex = 0;
			this.label4.Text = "Radius:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// RectanglePanel
			// 
			this.RectanglePanel.Controls.Add(this.HeightTextBox);
			this.RectanglePanel.Controls.Add(this.label9);
			this.RectanglePanel.Controls.Add(this.WidthTextBox);
			this.RectanglePanel.Controls.Add(this.label8);
			this.RectanglePanel.Location = new System.Drawing.Point(8, 80);
			this.RectanglePanel.Name = "RectanglePanel";
			this.RectanglePanel.Size = new System.Drawing.Size(184, 144);
			this.RectanglePanel.TabIndex = 12;
			// 
			// HeightTextBox
			// 
			this.HeightTextBox.Location = new System.Drawing.Point(64, 24);
			this.HeightTextBox.MaxLength = 40;
			this.HeightTextBox.Name = "HeightTextBox";
			this.HeightTextBox.Size = new System.Drawing.Size(120, 20);
			this.HeightTextBox.TabIndex = 17;
			this.HeightTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(0, 24);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(64, 20);
			this.label9.TabIndex = 16;
			this.label9.Text = "Height:";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// WidthTextBox
			// 
			this.WidthTextBox.Location = new System.Drawing.Point(64, 0);
			this.WidthTextBox.MaxLength = 40;
			this.WidthTextBox.Name = "WidthTextBox";
			this.WidthTextBox.Size = new System.Drawing.Size(120, 20);
			this.WidthTextBox.TabIndex = 15;
			this.WidthTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(0, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(64, 20);
			this.label8.TabIndex = 14;
			this.label8.Text = "Width:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TrianglePanel
			// 
			this.TrianglePanel.Controls.Add(this.CYTextBox);
			this.TrianglePanel.Controls.Add(this.label13);
			this.TrianglePanel.Controls.Add(this.CXTextBox);
			this.TrianglePanel.Controls.Add(this.label14);
			this.TrianglePanel.Controls.Add(this.BYTextBox);
			this.TrianglePanel.Controls.Add(this.label6);
			this.TrianglePanel.Controls.Add(this.BXTextBox);
			this.TrianglePanel.Controls.Add(this.label10);
			this.TrianglePanel.Controls.Add(this.AYTextBox);
			this.TrianglePanel.Controls.Add(this.label11);
			this.TrianglePanel.Controls.Add(this.AXTextBox);
			this.TrianglePanel.Controls.Add(this.label12);
			this.TrianglePanel.Location = new System.Drawing.Point(8, 80);
			this.TrianglePanel.Name = "TrianglePanel";
			this.TrianglePanel.Size = new System.Drawing.Size(184, 144);
			this.TrianglePanel.TabIndex = 13;
			// 
			// CYTextBox
			// 
			this.CYTextBox.Location = new System.Drawing.Point(64, 120);
			this.CYTextBox.MaxLength = 40;
			this.CYTextBox.Name = "CYTextBox";
			this.CYTextBox.Size = new System.Drawing.Size(120, 20);
			this.CYTextBox.TabIndex = 29;
			this.CYTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.CYTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TriangleTextBoxValidating);
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(0, 120);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(64, 20);
			this.label13.TabIndex = 28;
			this.label13.Text = "Third Y:";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// CXTextBox
			// 
			this.CXTextBox.Location = new System.Drawing.Point(64, 96);
			this.CXTextBox.MaxLength = 40;
			this.CXTextBox.Name = "CXTextBox";
			this.CXTextBox.Size = new System.Drawing.Size(120, 20);
			this.CXTextBox.TabIndex = 27;
			this.CXTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.CXTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TriangleTextBoxValidating);
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(0, 96);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(64, 20);
			this.label14.TabIndex = 26;
			this.label14.Text = "Third X:";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// BYTextBox
			// 
			this.BYTextBox.Location = new System.Drawing.Point(64, 72);
			this.BYTextBox.MaxLength = 40;
			this.BYTextBox.Name = "BYTextBox";
			this.BYTextBox.Size = new System.Drawing.Size(120, 20);
			this.BYTextBox.TabIndex = 25;
			this.BYTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.BYTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TriangleTextBoxValidating);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(0, 72);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 20);
			this.label6.TabIndex = 24;
			this.label6.Text = "Second Y:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// BXTextBox
			// 
			this.BXTextBox.Location = new System.Drawing.Point(64, 48);
			this.BXTextBox.MaxLength = 40;
			this.BXTextBox.Name = "BXTextBox";
			this.BXTextBox.Size = new System.Drawing.Size(120, 20);
			this.BXTextBox.TabIndex = 23;
			this.BXTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.BXTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TriangleTextBoxValidating);
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(0, 48);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(64, 20);
			this.label10.TabIndex = 22;
			this.label10.Text = "Second X:";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// AYTextBox
			// 
			this.AYTextBox.Location = new System.Drawing.Point(64, 24);
			this.AYTextBox.MaxLength = 40;
			this.AYTextBox.Name = "AYTextBox";
			this.AYTextBox.Size = new System.Drawing.Size(120, 20);
			this.AYTextBox.TabIndex = 21;
			this.AYTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.AYTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TriangleTextBoxValidating);
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(0, 24);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(64, 20);
			this.label11.TabIndex = 20;
			this.label11.Text = "First Y:";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// AXTextBox
			// 
			this.AXTextBox.Location = new System.Drawing.Point(64, 0);
			this.AXTextBox.MaxLength = 40;
			this.AXTextBox.Name = "AXTextBox";
			this.AXTextBox.Size = new System.Drawing.Size(120, 20);
			this.AXTextBox.TabIndex = 19;
			this.AXTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.AXTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TriangleTextBoxValidating);
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(0, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(64, 20);
			this.label12.TabIndex = 18;
			this.label12.Text = "First X:";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 20);
			this.label2.TabIndex = 6;
			this.label2.Text = "Center X:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// XTextBox
			// 
			this.XTextBox.Location = new System.Drawing.Point(72, 32);
			this.XTextBox.MaxLength = 40;
			this.XTextBox.Name = "XTextBox";
			this.XTextBox.Size = new System.Drawing.Size(120, 20);
			this.XTextBox.TabIndex = 8;
			this.XTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.XTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.CenterTextBoxValidating);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 20);
			this.label3.TabIndex = 9;
			this.label3.Text = "Center Y:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// YTextBox
			// 
			this.YTextBox.Location = new System.Drawing.Point(72, 56);
			this.YTextBox.MaxLength = 40;
			this.YTextBox.Name = "YTextBox";
			this.YTextBox.Size = new System.Drawing.Size(120, 20);
			this.YTextBox.TabIndex = 10;
			this.YTextBox.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			this.YTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.CenterTextBoxValidating);
			// 
			// FigureEditControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.YTextBox);
			this.Controls.Add(this.RectanglePanel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.XTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.CirclePanel);
			this.Controls.Add(this.rndButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.figureComboBox);
			this.Controls.Add(this.TrianglePanel);
			this.Name = "FigureEditControl";
			this.Size = new System.Drawing.Size(197, 260);
			this.CirclePanel.ResumeLayout(false);
			this.CirclePanel.PerformLayout();
			this.RectanglePanel.ResumeLayout(false);
			this.RectanglePanel.PerformLayout();
			this.TrianglePanel.ResumeLayout(false);
			this.TrianglePanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
