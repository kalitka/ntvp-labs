﻿using System;
using System.Windows.Forms;
using Model;
using System.Linq;

namespace GUI
{
	/// <summary>
	/// Form that allows editing data of IGeometricFigure ancestors.
	/// </summary>
	public partial class FigureForm : Form
	{
		/// <summary>
		/// Field to store figure generated from form data.
		/// </summary>
		private IGeometricFigure _figure;
		
		/// <summary>
		/// Property to access figure from outside.
		/// </summary>
		public IGeometricFigure Result 
		{
			get { return _figure; }
		}
		
		/// <summary>
		/// Constructor without arguments to use this form as a new figure creator
		/// </summary>
		public FigureForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			Text = "Add figure";
			DialogResult = DialogResult.Cancel;
			figureEditControl1.MyValidatedEvent += new EventHandler(this.FigureEditControlValidation);
		}
		
		/// <summary>
		/// Constructor that loads a figure's data onto form.
		/// </summary>
		/// <param name="figureToEdit">Figure to get data from.</param>
		public FigureForm(IGeometricFigure figureToEdit)
		{
			InitializeComponent();
			
			Text = "Edit figure";
			figureEditControl1.Figure = figureToEdit;
			
			DialogResult = DialogResult.Cancel;
		}
		
		/// <summary>
		/// Called on OK click, sets _figure to constructed figure, and DialogResult to OK.
		/// </summary>
		/// <param name="sender">Event sender, OKButton.</param>
		/// <param name="e">Event arguments.</param>
		private void OKButtonClick(object sender, EventArgs e)
		{
			try 
			{
				_figure = figureEditControl1.Figure;
				DialogResult = DialogResult.OK;
				Close();
			} 
			catch (ArgumentException ex) 
			{
				MessageBox.Show(String.Format("Sorry, but unable to create figure, please check your data.\n{0}", ex.Message), "DATA ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		/// <summary>
		/// Called on Cancel click, closes the form.
		/// </summary>
		/// <param name="sender">Event sender, CancelButton.</param>
		/// <param name="e">Event arguments.</param>
		private void CancelButtonClick(object sender, EventArgs e)
		{
			Close();
		}
		
		/// <summary>
		/// Updates OK button state on edit control changes.
		/// </summary>
		/// <param name="sender">Event sender, figureEditControl1.</param>
		/// <param name="e">Event arguments, null.</param>
		private void FigureEditControlValidation(object sender, EventArgs e)
		{
			OKButton.Enabled = figureEditControl1.IsValid;
		}
		
	}
}
