﻿using System;
using System.Windows;

namespace Model
{
	/// <summary>
	/// A class to represent circles. Implements IGeometricFigure.
	/// Holds radius and center point.
	/// </summary>
	public class Circle : IGeometricFigure
	{
		/// <summary>
		/// Internal representation of radius. Greater than zero. Less than Util.MaxValue.
		/// </summary>
		private double _radius;
		
		/// <summary>
		/// Internal representation of center point.
		/// </summary>
		private Point _center;
		
		/// <summary>
		/// Radius property. Forces "greater than zero" rule for radius.
		/// Throws an ArgumentException on non-positive values in setter.
		/// </summary>
		public double Radius 
		{
			get { return _radius; }
			set {
				if (!Util.IsValidPositive(value))
				{
					throw new ArgumentException(String.Format("Circle radius must be greater than zero, but lesser than {0}.", Util.MaxValue));
				} 
				_radius = value;
			}
		}
		
		/// <summary>
		/// Center property. No additional code requried.
		/// </summary>
		public Point Center { 
			get { return _center; }
			set {
				if (!Util.IsValid(value))
				{
					throw new ArgumentException(String.Format("Point coordinates must lie within {0}..{1}", Util.MinValue, Util.MaxValue));
				}
				_center = value;
			}
		}
		
		/// <summary>
		/// Type property: string "Circle"
		/// </summary>
		public String Type 
		{
			get { return "Circle"; }
		}
		
		/// <summary>
		/// Area property
		/// </summary>
		public double Area 
		{
			get { return Math.PI * Radius * Radius; }
		}
		
		/// <summary>
		/// Perimter property
		/// </summary>
		public double Perimeter 
		{
			get { return 2 * Math.PI * Radius; }
		}
		
		/// <summary>
		/// Circle constructor with variable positive radius. Center is set to (0; 0).
		/// </summary>
		/// <param name="radius">Radius of the circle. Must be greater than zero,
		/// otherwise it's setter will throw an ArgumentException.</param>
		public Circle(double radius)
		{
			Radius = radius;
			Center = new Point(0, 0);
		}
		
		/// <summary>
		/// Circle constructor with both radius and center.
		/// </summary>
		/// <param name="radius">Radius of the circle. Must be greater than zero,
		/// otherwise it's setter will throw an ArgumentException.</param>
		/// <param name="center">Center of the circle.</param>
		public Circle(double radius, Point center)
		{
			Radius = radius;
			Center = center;
		}
	}
}
