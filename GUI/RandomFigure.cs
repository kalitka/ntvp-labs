﻿using System;
using System.Windows;
using Model;

namespace GUI
{
	/// <summary>
	/// Class that generates random figures that implement IGeometricFigure.
	/// </summary>
	public class RandomFigure
	{
		/// <summary>
		/// Enum that holds all kinds of figures that can be generated.
		/// </summary>
		public enum possible_figures {CIRCLE, RECTANGLE, TRIANGLE};
		
		/// <summary>
		/// Default limiter, used when user provided none.
		/// </summary>
		private const double DEFAULT_LIMIT = 5.0d;
		
		/// <summary>
		/// Random generator used throughout the class.
		/// </summary>
		private static Random rnd;
		
		/// <summary>
		/// Default constructor with zero arguments.
		/// Random's seed is time based.
		/// </summary>
		public RandomFigure()
		{
			rnd = new Random();
		}
		
		/// <summary>
		/// Constructor with random seed.
		/// </summary>
		/// <param name="seed">Seed to random generator.</param>
		public RandomFigure(int seed)
		{
			rnd = new Random(seed);
		}
		
		/// <summary>
		/// Main method of the class, random figure generation.
		/// </summary>
		/// <param name="limit">Maximum absolute value of figure parameters.</param>
		/// <returns>Random geometric figure.</returns>
		public IGeometricFigure NextFigure(double limit)
		{
			// it's ugly in C#, see https://stackoverflow.com/questions/3132126/
			// in python it's "random.choice(possible_figures)"
			// then again, I could use ints for switch, but whatever
			var type = (possible_figures)rnd.Next(Enum.GetNames(typeof(possible_figures)).Length);
			
			switch (type) {
				case possible_figures.CIRCLE:
					var radius = NextDouble(limit);
					var center = NextPoint(limit);
					try {
						var circle = new Circle(radius, center);
						return circle;
					} catch (ArgumentException) {
						return NextFigure(limit);
					}
				case possible_figures.RECTANGLE:
					var topleft = NextPoint(limit);
					var width = NextDouble(limit);
					var height = NextDouble(limit);
					try {
						var rect = new Rectangle(topleft, width, height);
						return rect;
					} catch (ArgumentException) {
						return NextFigure(limit);
					}
				case possible_figures.TRIANGLE:
					var pointA = NextPoint(limit);
					var pointB = NextPoint(limit);
					var pointC = NextPoint(limit);
					try {
						var triangle = new Triangle(pointA, pointB, pointC);
						return triangle;
					} catch (ArgumentException) {
						return NextFigure(limit);
					}
				default:
					throw new NotImplementedException("Unknown type in possible_figures enum");
			}
		}
		
		/// <summary>
		/// Generator call without defined value limits,
		/// defaults to DEFAULT_LIMIT constant.
		/// </summary>
		/// <returns>Random geometric figure.</returns>
		public IGeometricFigure NextFigure()
		{
			return NextFigure(DEFAULT_LIMIT);
		}
		
		// code risen from the ashes below
		
		/// <summary>
		/// Random double generation.
		/// </summary>
		/// <param name="max">Maximum value of generated double.</param>
		/// <returns>Random double from 0 to max.</returns>
		private static double NextDouble(double max)
		{
			// still rounding for pretty output
			return Math.Round(max * rnd.NextDouble(), 1);
		}
		
		/// <summary>
		/// Random Point generation.
		/// </summary>
		/// <param name="max_coord">Maximum absolute value of coordinates.</param>
		/// <returns>Random Point with both coordinates between -max_coord and +max_coord.</returns>
		private static Point NextPoint(double max_coord)
		{
			var x = NextDouble(max_coord);
			if (rnd.NextDouble() < 0.5)
			{
				x *= -1;
			}
			var y = NextDouble(max_coord);
			if (rnd.NextDouble() < 0.5)
			{
				y *= -1;
			}
			return new Point(x, y);
		}

	}
}
