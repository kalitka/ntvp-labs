﻿using System;
using System.Windows;

namespace Model
{
	/// <summary>
	/// A class to represent rectangles. Implements IGeometricFigure.
	/// Holds top-left point, width and height.
	/// Code in this class assumes that right is positive X and
	/// top is positive Y. Things will break if other axes layout is chosen.
	/// </summary>
	public class Rectangle : IGeometricFigure
	{
		/// <summary>
		/// Top-left point of the rectangle as internal field.
		/// </summary>
		private Point _topLeft;
		
		/// <summary>
		/// Width of the rectangle. Greater than zero. Less than Util.MaxValue.
		/// </summary>
		private double _width;
		
		/// <summary>
		/// Width of the rectangle. Greater than zero. Less than Util.MaxValue.
		/// </summary>
		private double _height;
		
		/// <summary>
		/// Width property, mostly there to enforce that width is greater than zero.
		/// Throws an ArgumentException on non-positive values in setter.
		/// </summary>
		public double Width 
		{
			get { return _width; }
			set {
				if (!Util.IsValidPositive(value))
				{
					throw new ArgumentException(String.Format("Rectangle width must be greater than zero, but lesser than {0}.", Util.MaxValue));
				}
				_width = value;
			}
		}
		
		/// <summary>
		/// Height property, mostly there to enforce that height is greater than zero.
		/// Throws an ArgumentException on non-positive values in setter.
		/// </summary>
		public double Height 
		{
			get { return _height; }
			set {
				if (!Util.IsValidPositive(value))
				{
					throw new ArgumentException(String.Format("Rectangle height must be greater than zero, but lesser than {0}.", Util.MaxValue));
				}
				_height = value;
			}
		}
		
		/// <summary>
		/// Center property, actually uses top-left for calculations.
		/// </summary>
		public Point Center 
		{
			get {
				// minus is there since center is lower than topleft
				var offset = new Vector(Width / 2, -Height / 2);
				return Point.Add(_topLeft, offset);
			}
			set {
				if (!Util.IsValid(value))
				{
					throw new ArgumentException(String.Format("Point coordinates must lie within {0}..{1}", Util.MinValue, Util.MaxValue));
				}
				// minus is there since topleft is leftier (?) than center
				var offset = new Vector(-Width / 2, Height / 2);
				_topLeft = Point.Add(value, offset);
			}
		}
		
		/// <summary>
		/// Type property: string "Rectangle"
		/// </summary>
		public String Type 
		{
			get { return "Rectangle"; }
		}
		
		/// <summary>
		/// Area property
		/// </summary>
		public double Area 
		{
			get { return Width * Height; }
		}
		
		/// <summary>
		/// Perimeter property
		/// </summary>
		public double Perimeter 
		{
			get { return 2 * (Width + Height); }
		}
		
		/// <summary>
		/// Rectangle constructor using top-left and bottom right Points.
		/// </summary>
		/// <param name="topleft">Top-left point of the triangle</param>
		/// <param name="bottomright">Bottom-right point of the triangle</param>
		public Rectangle(Point topleft, Point bottomright)
		{
			_topLeft = topleft;
			var difference = Point.Subtract(bottomright, topleft);
			Width = difference.X;
			// height must be positive, but we're subtracting greater value from lesser one
			Height = -difference.Y;
			
			if (!Util.IsValid(Center))
			{
				throw new ArgumentException("Center point is out of limits.");
			}
		}
		
		/// <summary>
		/// Rectangle constructor using top-left Point and dimensions.
		/// </summary>
		/// <param name="topleft">Top-left point of the rectangle.</param>
		/// <param name="width">Positive width of the rectangle. Property setter throws an ArgumentException when width is zero and below.</param>
		/// <param name="height">Positive height of the rectangle. Property setter throws an ArgumentException when height is zero and below.</param>
		public Rectangle(Point topleft, double width, double height)
		{
			_topLeft = topleft;
			Width = width;
			Height = height;
			
			if (!Util.IsValid(Center))
			{
				throw new ArgumentException("Center point is out of limits.");
			}
		}
		
		/// <summary>
		/// Rectangle constructor using dimensions and center point.
		/// </summary>
		/// <param name="width">Positive width of the rectangle. Property setter throws an ArgumentException when width is zero and below.</param>
		/// <param name="height">Positive height of the rectangle. Property setter throws an ArgumentException when height is zero and below.</param>
		/// <param name="center">Center of the rectangle.</param>
		public Rectangle(double width, double height, Point center)
		{
			Width = width;
			Height = height;
			Center = center;
		}
	}
}
