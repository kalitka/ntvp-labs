﻿using System;
using System.Windows;

namespace Model
{
	/// <summary>
	/// A class to represent triangles. Implements IGeometricFigure.
	/// Holds its three points.
	/// </summary>
	public class Triangle : IGeometricFigure
	{
		/// <summary>
		/// Tolerance value for double calculations.
		/// If two doubles differ less than that value, they are considered equal.
		/// </summary>
		private const double _tolerance = 1e-5;
		
		/// <summary>
		/// First point of the triangle. The order is arbitrary.
		/// </summary>
		private Point _pointA;
		
		/// <summary>
		/// Second point of the triangle. The order is arbitrary.
		/// </summary>
		private Point _pointB;
		
		/// <summary>
		/// Third point of the triangle. The order is arbitrary.
		/// </summary>
		private Point _pointC;
		
		/// <summary>
		/// Center of the triangle as an intersection of its medians.
		/// </summary>
		public Point Center 
		{
			get {
				// we can substract two Points, but can't add them,
				// so here comes the dumb approach
				var x = (_pointA.X + _pointB.X + _pointC.X) / 3;
				var y = (_pointA.Y + _pointB.Y + _pointC.Y) / 3;
				return new Point(x, y);
			}
			set {
				if (!Util.IsValid(value))
				{
					throw new ArgumentException(String.Format("Point coordinates must lie within {0}..{1}", Util.MinValue, Util.MaxValue));
				}
				var oldCenter = Center;
				var offsetA = Point.Subtract(_pointA, oldCenter);
				_pointA = Point.Add(value, offsetA);
				var offsetB = Point.Subtract(_pointB, oldCenter);
				_pointB = Point.Add(value, offsetB);
				var offsetC = Point.Subtract(_pointC, oldCenter);
				_pointC = Point.Add(value, offsetC);
			}
		}
		
		/// <summary>
		/// Type property: string "Triangle"
		/// </summary>
		public String Type 
		{
			get { return "Triangle"; }
		}
		
		/// <summary>
		/// Area property
		/// </summary>
		public double Area 
		{
			get { return GetArea(); }
		}
		
		/// <summary>
		/// Perimter property
		/// </summary>
		public double Perimeter 
		{
			get { return GetPerimeter(); }
		}
		
		/// <summary>
		/// The one and only triangle constructor, no overloads there.
		/// Takes three Points as triangle's vertices. Throws an exception
		/// if the triangle degenerates to a line segment.
		/// </summary>
		/// <param name="a">First point of a triangle. Point ordering is arbitrary.</param>
		/// <param name="b">Second point of a triangle.</param>
		/// <param name="c">Third point of a triangle.</param>
		public Triangle(Point a, Point b, Point c)
		{
			
			if (!Util.IsValid(a) || !Util.IsValid(b) || !Util.IsValid(c))
			{
				throw new ArgumentException(String.Format("Point coordinates must lie within {0}..{1}", Util.MinValue, Util.MaxValue));
			}
			
			_pointA = a;
			_pointB = b;
			_pointC = c;
			
			// Triangle degeneration check, see
			// https://ru.stackoverflow.com/questions/55734/
			if (Math.Abs((c.X - a.X) * (b.Y - a.Y) - (b.X - a.X) * (c.Y - a.Y)) <= _tolerance)
			{
				throw new ArgumentException("Triangle points must not lie on one line");
			}
		}
		
		/// <summary>
		/// Calculates the area of the triangle using Heron's formula.
		/// </summary>
		/// <returns>Area of the triangle.</returns>
		private double GetArea()
		{
			// Heron's formula
			// Not quite optimized,
			// but readabilty and simplicity > speed
			// (a least here)
			var s = GetPerimeter() / 2;
			var a = Point.Subtract(_pointB, _pointA).Length;
			var b = Point.Subtract(_pointC, _pointB).Length;
			var c = Point.Subtract(_pointA, _pointC).Length;
			
			return Math.Sqrt(s * (s - a) * (s - b) * (s - c));
		}
		
		/// <summary>
		/// Calculates the perimeter of the triangle.
		/// </summary>
		/// <returns>Perimeter of the triangle.</returns>
		private double GetPerimeter()
		{
			var sideAB = Point.Subtract(_pointB, _pointA);
			var sideBC = Point.Subtract(_pointC, _pointB);
			var sideCA = Point.Subtract(_pointA, _pointC);
			return sideAB.Length + sideBC.Length + sideCA.Length;
		}
		
		/// <summary>
		/// Dirty hack to access private _point* fields from outside.
		/// </summary>
		/// <returns>Double array: 1.X, 1.Y, 2.X, 2.Y, 3.X, 3.Y, where 1, 2, 3 - triangle points.</returns>
		public double[] GetPoints()
		{
			return new double[] {_pointA.X, _pointA.Y, _pointB.X, _pointB.Y, _pointC.X, _pointC.Y};
		}
	}
}
